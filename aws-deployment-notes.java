// [SECTION] AWS Deployment

/*
1.   Logging in to IAM Account
  	- login using the IAM account provided to you.
  	- https://436816677911.signin.aws.amazon.com/console
  	- You will be prompt to change the temporary password, make sure that it follows the following:
  		- Atleast one uppercase letter
  		- Atleast one numeric character
*/

/*
2. Selecting a Region
	- Services are tied to a selected region. Before proceeding, select the Oregon (us-west-2) region.
*/

/*
3. Launch an EC2 Instance
	- Go to the EC2 Management Console and select the Launch Instance button.
		- You may provide this link: https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#Home:
		- or click EC2 instance
		- Alternatively, you can go directly to the Launch Instance Wizard: https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#LaunchInstances
*/

/*
4.  Creeating an EC2 instance

  Step 1: Add an instance name
    - Format: bucket-batch-class_num-lastName-instance (class_num as the class number, e.g cit01)

  Step 2: Choose an Amazon Machine Image (AMI)
    - Select the "Ubuntu Server", the latest version of the server is at Ubuntu 22.04

  Step 3: Choose an Instance Type
    - Select the t2.micro instance type which is eligible for free tier usage. (which is selected by default)

  Step 4.1: Key Pair Configuration (skip this step if Key pair is already created)
    - Create a new "key-pair" which will be used to connect to your instance.
    - "Key pair name" should be similar name with your instance (bucket-batch-class_num-lastName-instance)
    - "Key pair type" is RSA
    - "Private key file format" is .pem
        - PEM means "Privacy Enhanced Mail" and is a file that contains cryptographic keys for securely connecting to our EC2 instance.
    - Click "Create key pair".

    Step 4.2: Key Pair Configuration
      - In the search bar, look for your created key pair.

  Step 5: Network setting
    - Under "Firewall(security group)", configure the following:
      - Choose "Create Security Group"
      - Check "Allow SSH traffic from Anywhere / 0.0.0.0"
      - Check "Allow HTTPS traffic from the internet"
      - Check "Allow HTTP traffic from the internet"

  Step 6: Review Instance Launch
    - Once the settings for the new instance have been verified, click the "Launch button".
    // Skip the steps below if the .pem file is already added in the .ssh file
    - After launching the instance a .pem file will be generated and downloaded. Transfer this in your ".ssh folder".
    - To locate your ".ssh/" folder, open a gitbash terminal and type "cd .ssh/" to change the directory.
    - Once you're in the ".ssh/" folder directory, type in the terminal the "explorer ." to open the file explorer.

  Step 7: Click the instance link
    - This show more details about the created instance.
*/

/*

5. Accessing the EC2 Instance
  - Click on "connect button" to access details about how to connect to the instance.
  - Open a terminal under your ".ssh/" folder and trigger the command under "Run this command, if necessary, to ensure your key is not publicly viewable.":

    chmod 400 bucket-batch-<class_num>-<lastname>-instance.pem

  - To connect to your AWS EC2 instance, run the command under the example:

    ssh -i "bucket-batch-<class_num>-<lastname>-instance.pem" ubuntu@<public-ipv4-address>.us-west-2.compute.amazonaws.com

  - Once connected to your instance, you may now install the necessary tools needed to run the Java Spring Boot API

*/

/*
6. Updating the package index files on the system, which contain information about available packages and their versions
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  sudo apt-get update

/*
7. Setting up the ssh-keys for the gitlab of your AWS E2 Instance.
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  ssh-keygen

/*
 Upon executing the command you will be prompt by the following:
    Enter file in which to save the key (/home/ubuntu/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:

    Don't type anything aand press "Enter"
*/

// Execute the following command to copy your generaterd ssh-key

  cat ~/.ssh/id_rsa.pub

  // Note: Because we are currently using an EC2 instance, this may not copy the ssh key on your clipboard, highlight the ssh key after entering the command and right click then copy it to your GitLab account.

/*
8. Install mariadb-server in the instance.
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

    sudo apt install mariadb-server

/*
9. Install mysql_secure_installation
  - "mysql_secure_installation" is a shell script developed for securing the MySQL server installation on Unix systems.
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  sudo mysql_secure_installation

  /*
    After Entering the command above you need to answer the followng security features:

    Switch to unix_socket authentication [Y/n]: n
    Change the root password? [Y/n]: n
    Remove anonymous users? [Y/n]: n
    Disallow root login remotely? [Y/n]: n
    Remove test database and access to it? [Y/n]: n
    Reload privilege tables now? [Y/n] Y

  */

/*
10. Accesing the "mariadb_server" in the AWS EC2 instance
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  sudo mysql

  /*
    - After entering the command, you will now have access with the maria_db shell.
    - Inside the maria_db shell, execute the following queries.
  */

  GRANT ALL ON *.* TO 'root'@'localhost' IDENTIFIED BY 'root' WITH GRANT OPTION;

  // The command above grants all privileges to the user "root" on all databases and tables (".") of the MySQL server running on the same machine ("localhost").

  FLUSH PRIVILEGES;

  // The command above is used to reload the server's privileges from the grant tables in the mysql system database.
  // Whenever privileges are added, modified or removed, the changes will not take effect until the server is restarted or the FLUSH PRIVILEGES command is run.

  CREATE DATABASE spring_blog;

  // Create the spring_blog database that will be use by the Spring blog API

  SHOW DATABASES;

  // To check if the database is created.

  // Close the maria_db shell by executing the following

  Ctrl + C or type exit

/*
11.  Installing maven in the AWS EC2 instance
*/
  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  sudo apt install maven // this will install maven dependency

  mvn -version // to check the maven installation and version

/*
12. Install Java v17 jdk and jre in the AWS EC2 instance
*/

    // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  // To install the development kit and runtime environment
  sudo apt install -y openjdk-17-jdk
  sudo apt install -y openjdk-17-jre

  java -version // to check the maven installation and version

/*
13. Installing "nginx" web server to serve are Java Spring blog API
*/

  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

  // To install the nginx web server which is a ubuntu based server.
  sudo apt install nginx

  // This command is used to start the nginx web server
  sudo service nginx start

/*
14. Clone the S16 resources in your AWS instance
*/

// Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

git clone git@gitlab.com:tuitt/ec-students/cit/cit-b002.git


/*
15. Updating the username and password credential for the mariadb/sql connection
*/

// Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:

sudo vi cit-b002/S16/discussion/src/main/resources/application.properties

/*
  - "sudo vi" is used to open the application.properties file in the Vi text editor.
  - To start editing the file press any keys in the keyboard and an "Insert" keyword will be shown in the lower left hand side of the terminal to indicate that we can type and edit the file.
*/

// Update the following in the application.properties
  spring.datasource.username = root
  spring.datasource.password = root //password will be changed to root

// Press "Esc" to stop editing

// In the terminal type the following command, to apply the changes in the file and exit the Vi editor

  :wq

/*
16. Traverse inside S16 discussion folder and create a deployable artifact using mvn.
*/
  // Make sure you’re connected to your AWS instance. Trigger the following commands in the instance connected terminal:


  //Change directory
  cd cit-b002/S16/discussion/

  mvn -DskipTests install

  // "-DskipTests" skip the unit tests when building the project
  // "install" is for compliling and packaging the project

/*
17. Start the Java Application and edit the Nginx configuration file in the AWS EC2 instance
*/

nohup java -jar target/discussion-0.0.1-SNAPSHOT.jar & sudo vi /etc/nginx/sites-enabled/default

//"nohup" is used to start the Java Application

/*
  Edit the Nginx configuration to add the localhost in the serve file

    - Comment the line with “try_files” by adding the pound symbol (#) before it.
    - Insert a new line after the “try_files” line and include the proxy pass value
*/
    //"proxy_pass" a configuration directive used to forward requests from a web server to another server.
    proxy_pass http://localhost:8080/;

    // - Press escape to stop editing
    // - Type :wq to exit the vi editor



// Restart the nginx server by executing the following command:

sudo service nginx restart

/*
18. Test the hosted spring blog app in the postman using IPv4 address.
*/

http://<public-IPv4-address>/users/register
